/**
 * Loads https://github.com/typhonjs-fvtt/eslint-config-foundry.js/blob/main/0.8.0.js
 * NPM: https://www.npmjs.com/package/@typhonjs-fvtt/eslint-config-foundry.js
 *
 * Note: specific versions are located in /<VERSION>
 */
module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: [
        '@typescript-eslint',
        'prettier',
        'html'
    ],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
        '@typhonjs-fvtt/eslint-config-foundry.js',
        'prettier'
    ],

    // Prevents overwriting any built in globals particularly from `@typhonjs-fvtt/eslint-config-foundry.js`. 
    // `event / window.event` shadowing is allowed due to being a common variable name and an uncommonly used browser 
    // feature.
    //
    // Note: if you are using Typescript you must use `@typescript-eslint/no-shadow`
    rules: {
        'no-shadow': ['error', { 'builtinGlobals': true, 'hoist': 'all', 'allow': ['event'] }],
        '@typescript-eslint/no-shadow': ['error', { 'builtinGlobals': true, 'hoist': 'all', 'allow': ['event'] }],
        'prettier/prettier': 'warn'
    }
}