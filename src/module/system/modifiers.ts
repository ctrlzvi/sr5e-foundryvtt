export interface ModifiableValue<T> {
    base: T,
    current: T,
}
