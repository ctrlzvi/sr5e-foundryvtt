import { Attribute } from "./attributes";

export interface Character {
    attributes: {
        body: Attribute,
        agility: Attribute,
        reaction: Attribute,
        strength: Attribute,
        willpower: Attribute,
        logic: Attribute,
        intuition: Attribute,
        charisma: Attribute,
        edge: Attribute,
        initiative: Attribute,
        magic: Attribute,
        resonance: Attribute,
        essence: Attribute,
    }
}