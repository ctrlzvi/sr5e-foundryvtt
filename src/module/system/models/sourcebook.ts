export interface SourceBookReference {
    book: SourceBook,
    pages: Pages[],
}

export interface SourceBook {
    name: string,
    abbreviation: string,

    /* Chummer data. */
    // The UUID Chummer assigned to the metatype.
    chummerId?: string,
}

export interface Pages {
    first: number,
    last: number,
}
