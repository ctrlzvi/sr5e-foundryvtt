import { SourceBookReference } from "./sourcebook";


// TODO (zeffron 2021-09-12) Convert this into a class so functionality isn't
// free functions.
export interface Metatype {
    // The metavariant component of the metatype.
    metavariant: Metavariant,
    // The shapeshifter component of the metatype.
    shapeshifter?: Metatype,
    // The attribute information for the metatype.
    attributes: {
        body: MetatypeAttribute,
        agility: MetatypeAttribute,
        reaction: MetatypeAttribute,
        strength: MetatypeAttribute,
        willpower: MetatypeAttribute,
        logic: MetatypeAttribute,
        intuition: MetatypeAttribute,
        charisma: MetatypeAttribute,
        edge: MetatypeAttribute,
        initiative: MetatypeAttribute,
        magic: MetatypeAttribute,
        resonance: MetatypeAttribute,
        essence: MetatypeAttribute,
    },

    /* Costs */
    // TODO (zeffron 2021-09-12) Add cost information (karma) to support
    // character creation.

    // Reference data.
    sourceBook?: SourceBookReference,

    /* Data specific to Chummer. */

    // The UUID Chummer assigned to the metatype.
    chummerId?: string,
    // The category Chummer assigned to the metatype.
    chummerCategory?: string,
    // Chummer marks which metatypes have special movement.
    movement?: 'Special',
}

export interface Metavariant {
    metasapient: string,
    variant: string,
}

export interface MetatypeAttribute {
    minimum: number,
    limit: number,
    augmentedLimit: number,
}

export type Shapeshifter = Metatype & { shapeshifter: Metatype };

// TODO (zeffron 2021-09-12) Convert this into a type guard for a Metatype
// class.
export function isShapeshifter(metatype: Metatype) {
    return metatype.shapeshifter !== undefined;
}

export const InvalidMetatype: Metatype = {
    metavariant: {
        metasapient: '',
        variant: '',
    },
    attributes: {
        body: { minimum: 0, limit: 0, augmentedLimit: 0 },
        agility: { minimum: 0, limit: 0, augmentedLimit: 0 },
        reaction: { minimum: 0, limit: 0, augmentedLimit: 0 },
        strength: { minimum: 0, limit: 0, augmentedLimit: 0 },
        willpower: { minimum: 0, limit: 0, augmentedLimit: 0 },
        logic: { minimum: 0, limit: 0, augmentedLimit: 0 },
        intuition: { minimum: 0, limit: 0, augmentedLimit: 0 },
        charisma: { minimum: 0, limit: 0, augmentedLimit: 0 },
        edge: { minimum: 0, limit: 0, augmentedLimit: 0 },
        initiative: { minimum: 0, limit: 0, augmentedLimit: 0 },
        magic: { minimum: 0, limit: 0, augmentedLimit: 0 },
        resonance: { minimum: 0, limit: 0, augmentedLimit: 0 },
        essence: { minimum: 0, limit: 0, augmentedLimit: 0 },
    }
}