import { ModifiableValue } from "../modifiers";

export interface Attribute extends ModifiableValue<number> {
}
