export interface Book {
    id: [string],
    name: [string],
    code: [string],
    hide?: [string],
}
