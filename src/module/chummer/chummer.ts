// import { parse } from 'fast-xml-parser';
import * as xml2js from 'xml2js';
import { Book as ChummerSourceBook } from './models/books';
import { Metatype as ChummerMetatype, Metavariant as ChummerMetavariant } from './models/metatypes';
import { SourceBook } from '../system/models/sourcebook';
import { Metatype } from '../system/models/metatype';
import { SYSTEM_NAME } from "../sr5e";

type Awaited<T> = T extends PromiseLike<infer U> ? Awaited<U> : T;

// ChummerData receives Chummer data and converts it into a consumable format.
export class ChummerData {
    private xmlData: File;

    constructor(xmlData: File) {
        this.xmlData = xmlData;
    }

    async parse(): Promise<{ books?: { [index: string]: SourceBook }, metatypes?: Metatype[] }> {
        const xml = await new xml2js.Parser().parseStringPromise(await this.xmlData.text());
        const data = xml?.chummer;
        if (data === undefined) {
            throw new Error(`${this.xmlData.name} is not a Chummer XML file`);
        }

        const retval: Awaited<ReturnType<typeof this.parse>> = {};
        for (const [key, value] of Object.entries(data)) {
            switch (key) {
                case 'metatypes':
                    // retval.metatypes.push(...this.parseMetatypes((value as { metatype: ChummerMetatype[] }).metatype));
                    const metatypes: ChummerMetatype[] = (value as [{ metatype: ChummerMetatype[] }])[0].metatype;
                    retval.metatypes = this.parseMetatypes(metatypes);
                    break;
                case 'books':
                    const books: ChummerSourceBook[] = (value as [{ book: ChummerSourceBook[] }])[0].book;
                    retval.books = this.parseBooks(books);
                    break;
            }
        }
        return retval;
    }

    private parseMetatypes(chummerMetatypes: ChummerMetatype[]): Metatype[] {
        // Chummer doesn't store the metasapient type with each metavariant
        // entry. Instead, it assumes the metasapient type from the parent of
        // the metavariant. This does not work well for the metahuman form of
        // Shapeshifters as the data Chummer provides does not include the
        // metasapient (this is not critical for most gameplay, but affects a
        // few things and is nice for completeness).
        // To resolve this, we keep a map from metavariant name to metasapient.
        // We use this to update the Shapeshifter metavariants when we are done.
        const metatypes: Metatype[] = [];
        const shapeshifters: Metatype[] = [];
        const metasapients: Map<string, string> = new Map();
        for (const chummerMetatype of chummerMetatypes) {
            const metasapient: string = chummerMetatype.name[0];
            const metatype = this.metatypeFromChummerMetavariant(chummerMetatype, metasapient);

            const shapeshifter: boolean = chummerMetatype.category !== undefined && chummerMetatype.category[0] === 'Shapeshifter';
            // Shapeshifters need to have an associated metavariant that they
            // shift from, so the base type does not get added.
            if (!shapeshifter) {
                metatypes.push(metatype)
                metasapients.set(metatype.metavariant.variant, metasapient);
            }

            if (chummerMetatype.metavariants !== undefined && chummerMetatype.metavariants[0] !== '') {
                for (const chummerMetavariant of chummerMetatype.metavariants[0].metavariant) {
                    if (!shapeshifter) {
                        const metavariant = this.metatypeFromChummerMetavariant(chummerMetavariant, metasapient);
                        metatypes.push(metavariant);
                        metasapients.set(metavariant.metavariant.variant, metasapient);
                    } else {
                        const metavariant = this.metatypeFromChummerMetavariant(chummerMetavariant, metasapient, metatype);
                        shapeshifters.push(metavariant);
                    }
                }
            }
        }

        // Now that all of the metavariants have been through their first pass,
        // we can update the metasapient type of the metahuman form of the
        // Shapeshifters.
        for (const shapeshifter of shapeshifters) {
            shapeshifter.metavariant.metasapient = metasapients.get(shapeshifter.metavariant.variant) ?? shapeshifter.metavariant.metasapient;
            metatypes.push(shapeshifter);
        }
        return metatypes;
    }

    private metatypeFromChummerMetavariant(metavariant: ChummerMetavariant, metasapient: string, shapeshifter?: Metatype): Metatype {
        const metatype: Metatype = {
            metavariant: {
                metasapient: metasapient,
                variant: metavariant.name[0],
            },
            shapeshifter: shapeshifter,
            attributes: {
                body: {
                    minimum: parseInt(metavariant.bodmin[0]),
                    limit: parseInt(metavariant.bodmax[0]),
                    augmentedLimit: parseInt(metavariant.bodaug[0]),
                },
                agility: {
                    minimum: parseInt(metavariant.agimin[0]),
                    limit: parseInt(metavariant.agimax[0]),
                    augmentedLimit: parseInt(metavariant.agiaug[0]),
                },
                reaction: {
                    minimum: parseInt(metavariant.reamin[0]),
                    limit: parseInt(metavariant.reamax[0]),
                    augmentedLimit: parseInt(metavariant.reaaug[0]),
                },
                strength: {
                    minimum: parseInt(metavariant.strmin[0]),
                    limit: parseInt(metavariant.strmax[0]),
                    augmentedLimit: parseInt(metavariant.straug[0]),
                },
                willpower: {
                    minimum: parseInt(metavariant.wilmin[0]),
                    limit: parseInt(metavariant.wilmax[0]),
                    augmentedLimit: parseInt(metavariant.wilaug[0]),
                },
                logic: {
                    minimum: parseInt(metavariant.logmin[0]),
                    limit: parseInt(metavariant.logmax[0]),
                    augmentedLimit: parseInt(metavariant.logaug[0]),
                },
                intuition: {
                    minimum: parseInt(metavariant.intmin[0]),
                    limit: parseInt(metavariant.intmax[0]),
                    augmentedLimit: parseInt(metavariant.intaug[0]),
                },
                charisma: {
                    minimum: parseInt(metavariant.chamin[0]),
                    limit: parseInt(metavariant.chamax[0]),
                    augmentedLimit: parseInt(metavariant.chaaug[0]),
                },
                edge: {
                    minimum: parseInt(metavariant.edgmin[0]),
                    limit: parseInt(metavariant.edgmax[0]),
                    augmentedLimit: parseInt(metavariant.edgaug[0]),
                },
                initiative: {
                    minimum: parseInt(metavariant.inimin[0]),
                    limit: parseInt(metavariant.inimax[0]),
                    augmentedLimit: parseInt(metavariant.iniaug[0]),
                },
                magic: {
                    minimum: parseInt(metavariant.magmin[0]),
                    limit: parseInt(metavariant.magmax[0]),
                    augmentedLimit: parseInt(metavariant.magaug[0]),
                },
                resonance: {
                    minimum: parseInt(metavariant.resmin[0]),
                    limit: parseInt(metavariant.resmax[0]),
                    augmentedLimit: parseInt(metavariant.resaug[0]),
                },
                essence: {
                    minimum: parseInt(metavariant.essmin[0]),
                    limit: parseInt(metavariant.essmax[0]),
                    augmentedLimit: parseInt(metavariant.essaug[0]),
                },
            },
            chummerId: metavariant.id[0],
        };
        if (metavariant.category !== undefined) {
            metatype.chummerCategory = metavariant.category[0];
        }

        // If we have a book and a page number, we will set the source
        // reference.
        if (metavariant.source !== undefined && metavariant.page !== undefined) {
            // If we don't have a book with this code, we'll make one up, and
            // the next time we load books, we'll try to replace it.
            const book = ((game as Game).settings.get(SYSTEM_NAME, 'SR5e.SourceBooks') as { [index: string]: SourceBook })[metavariant.source[0]] ?? {
                name: metavariant.source[0],
                abbreviation: metavariant.source[0]
            };
            metatype.sourceBook = {
                book: book,
                pages: [{ first: parseInt(metavariant.page[0]), last: parseInt(metavariant.page[0]) }],
            }
        }
        return metatype;
    }

    private parseBooks(books: ChummerSourceBook[]): { [index: string]: SourceBook } {

        return Object.fromEntries(
            books.map((book) => [
                book.code[0],
                {
                    name: book.name[0],
                    abbreviation: book.code[0],
                    chummerId: book.id[0],
                }
            ])
        );
    }
}
