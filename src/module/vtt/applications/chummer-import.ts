import { SYSTEM_NAME } from '../../sr5e';
import { ChummerData } from '../../chummer/chummer'
import { MetatypeImporter } from '../importers/metatype';

export class ChummerImport extends FormApplication<FormApplication.Options, {}> {
    private chummerData: Map<string, ChummerData> = new Map();

    static override get defaultOptions(): FormApplication.Options {
        return mergeObject(super.defaultOptions, {
            classes: ['form'],
            popOut: true,
            template: 'systems/sr5e/dist/templates/applications/chummer-import.html',
            id: 'chummer-import',
            title: 'Import Chummer Data',
            dragDrop: [{ dropSelector: '.drop-zone' }],
        });
    }

    override getData(): {} {
        return {
            data: Array.from(this.chummerData.keys()),
        };
    }

    protected override async _updateObject(_event: Event, _formData?: unknown): Promise<void> {
        // TODO(zeffron 2021-09-12) We want to parse in a specific order so
        // references are ready.
        for (const [_, data] of this.chummerData) {
            const parsedData = await data.parse();
            if (parsedData.books !== undefined) {
                (game as Game).settings.set(SYSTEM_NAME, 'SR5e.SourceBooks', parsedData.books);
            }
            if (parsedData.metatypes !== undefined) {
                new MetatypeImporter().import(parsedData.metatypes, 'Chummer');
            }
        }
    }

    override _onDrop(event: DragEvent): void {
        event.preventDefault();
        if (event.dataTransfer === null) {
            return;
        }

        for (const file of event.dataTransfer.files) {
            if (file.type !== 'text/xml' && file.type !== 'application/xml') {
                console.warn(`${file.name} is not an XML file`);
                continue;
            }

            this.chummerData.set(file.name, new ChummerData(file));
            this.render(false);
        }
    }

    override _onDragOver(event: DragEvent): void {
        event.preventDefault();
    }
}