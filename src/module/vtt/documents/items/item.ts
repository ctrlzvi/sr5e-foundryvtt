// import { ActiveEffectDataConstructorData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/activeEffectData";
import { ItemDataConstructorData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs/itemData";
import { Context } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/document.mjs";
import { Metatype } from "../../../system/models/metatype";
// import { ACTIVE_EFFECT_MODES } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/constants.mjs";

export class SR5eItem extends Item {
    constructor(data?: ItemDataConstructorData, context?: Context<Actor>) {
        // TODO (zeffron 2021-09-13) Move this to the import of Chummer data?
        if (data !== undefined) {
            super({
                ...data, effects: [{
                    changes: Object.entries((data.data as Metatype).attributes).map(
                        ([attribute, value]) => ({
                            key: `data.attributes.${attribute}.current`,
                            value: String(value.minimum),
                            mode: 2, // ACTIVE_EFFECT_MODES.ADD
                        })
                    ),
                    label: data.name,
                    transfer: true,
                }]
            })
        } else {
            super(data, context);
        }
    }

    override prepareData(): void {
        super.prepareData();
    }

    // Although it's nice that we have the stats recorded, and we could grab
    // them from the actor to apply them to attributes, we're going to instead
    // use effects to make the changes. By using effects we can keep the logic
    // for making changes local to the cause of the change, rather than making
    // an outside consumer know about internal logic.
    override prepareBaseData(): void {
        super.prepareBaseData();
    }
}
