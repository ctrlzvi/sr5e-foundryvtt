import { ActorData } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/data/data.mjs";
import { DocumentModificationOptions } from "@league-of-foundry-developers/foundry-vtt-types/src/foundry/common/abstract/document.mjs";
import { Character } from "../../../system/models/character";

// Apparently we can only have one type of Actor for the entire system (even
// though we can have many, many item types. Seems a bit weird, but let's try
// to embrace it.)

export class SR5eActor extends Actor {
    override prepareData(): void {
        // Prepare data for the actor. Calling the super version of this executes
        // the following, in order: data reset (to clear active effects),
        // prepareBaseData(), prepareEmbeddedDocuments() (including active effects),
        // prepareDerivedData().
        super.prepareData();
    }

    // We use this to copy base data to current data before effects are
    // applied. This lets the base data value be persisted intead of being
    // altered by the effects, but creates a non-derived value that is adjusted
    // by the effects.
    override prepareBaseData(): void {
        // Data modifications in this step occur before processing embedded
        // documents or derived data
        switch (this.data.type) {
            case 'character':
                const characterData = this.data.data as Character;
                for (const attribute of Object.values(characterData.attributes)) {
                    attribute.current = attribute.base;
                }
        }
    }

    /**
   * Augment the basic actor data with additional dynamic data. Typically,
   * you'll want to handle most of your calculated/derived data in this step.
   * Data calculated in this step should generally not exist in template.json
   * (such as ability modifiers rather than ability scores) and should be
   * available both inside and outside of character sheets (such as if an actor
   * is queried and has a roll executed directly from it).
   */
    override prepareDerivedData(): void {
        const actorData = this.data;

        // Make separate methods for each Actor type (character, npc, etc.) to keep
        // things organized.
        switch (actorData.type) {
            case 'character':
                this.prepareCharacterData(actorData);
                break;
            case 'critter':
                this.prepareCritterData(actorData);
                break;
            case 'done':
                this.prepareDroneData(actorData);
                break;
            case 'ic':
                this.prepareICData(actorData);
                break;
            case 'spirit':
                this.prepareSpiritData(actorData);
                break;
            case 'sprite':
                this.prepareSpriteData(actorData);
                break;
            case 'vehicle':
                this.prepareVehicleData(actorData);
                break;
        }
    }

    private prepareCharacterData(actorData: ActorData): void {
        if (actorData.type !== 'character') {
            throw new TypeError(`Actor of type ${actorData.type} passed to character function`);
        }
    }

    private prepareCritterData(actorData: ActorData): void {
        if (actorData.type !== 'critter') {
            throw new TypeError(`Actor of type ${actorData.type} passed to critter function`);
        }
    }

    private prepareDroneData(actorData: ActorData): void {
        if (actorData.type !== 'drone') {
            throw new TypeError(`Actor of type ${actorData.type} passed to drone function`);
        }
    }

    private prepareICData(actorData: ActorData): void {
        if (actorData.type !== 'ic') {
            throw new TypeError(`Actor of type ${actorData.type} passed to ic function`);
        }
    }

    private prepareSpiritData(actorData: ActorData): void {
        if (actorData.type !== 'spirit') {
            throw new TypeError(`Actor of type ${actorData.type} passed to spirit function`);
        }
    }

    private prepareSpriteData(actorData: ActorData): void {
        if (actorData.type !== 'sprite') {
            throw new TypeError(`Actor of type ${actorData.type} passed to sprite function`);
        }
    }

    private prepareVehicleData(actorData: ActorData): void {
        if (actorData.type !== 'vehicle') {
            throw new TypeError(`Actor of type ${actorData.type} passed to vehicle function`);
        }
    }

    override getRollData(): this['data']['data'] {
        const data = super.getRollData();

        // Make separate methods for each Actor type (character, npc, etc.) to keep
        // things organized.
        switch (this.data.type) {
            case 'character':
                this.getCharacterRollData(data);
                break;
            case 'critter':
                this.getCritterRollData(data);
                break;
            case 'done':
                this.getDroneRollData(data);
                break;
            case 'ic':
                this.getICRollData(data);
                break;
            case 'spirit':
                this.getSpiritRollData(data);
                break;
            case 'sprite':
                this.getSpriteRollData(data);
                break;
            case 'vehicle':
                this.getVehicleRollData(data);
                break;
        }

        return data;
    }

    private getCharacterRollData(data: this['data']['data']): void {
        if (this.data.type !== 'character') {
            throw new TypeError(`Actor of type ${this.data.type} passed to character function`);
        }

        data;
    }

    private getCritterRollData(data: this['data']['data']): void {
        if (this.data.type !== 'critter') {
            throw new TypeError(`Actor of type ${this.data.type} passed to critter function`);
        }

        data;
    }

    private getDroneRollData(data: this['data']['data']): void {
        if (this.data.type !== 'drone') {
            throw new TypeError(`Actor of type ${this.data.type} passed to drone function`);
        }

        data;
    }

    private getICRollData(data: this['data']['data']): void {
        if (this.data.type !== 'ic') {
            throw new TypeError(`Actor of type ${this.data.type} passed to ic function`);
        }

        data;
    }

    private getSpiritRollData(data: this['data']['data']): void {
        if (this.data.type !== 'character') {
            throw new TypeError(`Actor of type ${this.data.type} passed to spirit function`);
        }

        data;
    }

    private getSpriteRollData(data: this['data']['data']): void {
        if (this.data.type !== 'sprite') {
            throw new TypeError(`Actor of type ${this.data.type} passed to sprite function`);
        }

        data;
    }

    private getVehicleRollData(data: this['data']['data']): void {
        if (this.data.type !== 'vehicle') {
            throw new TypeError(`Actor of type ${this.data.type} passed to vehicle function`);
        }

        data;
    }

    // Unlike most Items, a character can only have one metatype (technically
    // two if they're a Shapeshifter, but the Shapeshifter metatype already
    // handles that.) When adding a new metatype to a character, if the
    // character, already has a metatype, we remove the previous metatype.
    override _preCreateEmbeddedDocuments(embeddedName: string, result: Record<string, unknown>[], _options: DocumentModificationOptions, _userId: string): void {
        // If we're not embedding an Item, we don't care about what we're
        // adding.
        if (embeddedName !== 'Item') {
            return;
        }

        // TODO (zeffron 2021-09-13) Add similar logic to items to prevent
        // adding a metatype to an item or a non-character metatype.
        const newMetatypes = result.filter(
            (document) => document.hasOwnProperty('type') && document['type'] === 'metatype'
        );

        if (newMetatypes.length > 1) {
            ui.notifications?.error(`Cannot add ${newMetatypes.length} metatypes to ${this.name}. ${this.type} can only have 1 metatype.`);

            // Remove all of the metatypes because we don't know which one the
            // user prefers.
            // TODO (zeffron 2021-09-13) To save on allocations (and thus time)
            // we can use Array.forEach with tracking current index.
            // See https://stackoverflow.com/questions/37318808/what-is-the-in-place-alternative-to-array-prototype-filter
            result.splice(
                0,
                result.length,
                ...result.filter(
                    (document) => !document.hasOwnProperty('type') || document['type'] !== 'metatype'
                )
            );
        } else if (newMetatypes.length == 1) {
            const oldMetatypes = this.itemTypes['metatype'] ?? [];
            this.deleteEmbeddedDocuments(
                'Item',
                oldMetatypes.map((metatype) => metatype.id).filter((id): id is string => id !== null),
            );
        }
    }
}
