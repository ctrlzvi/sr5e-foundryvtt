export class SR5eActorSheet extends ActorSheet {
    static override get defaultOptions(): ActorSheet.Options {
        return mergeObject(super.defaultOptions, {
            classes: ['sr5e', 'sheet', 'actor'],
            template: 'systems/sr5e/dist/templates/actors/character.html',
            width: 600,
            height: 600,
            tabs: [
                {
                    navSelector: '.sheet-tabs',
                    contentSelector: '.sheet-body',
                    initial: 'personal-data'
                }
            ],
        });
    }

    override getData(): ActorSheet.Data<ActorSheet.Options> | Promise<ActorSheet.Data<ActorSheet.Options>> {
        // Retrieve the data structure from the base sheet. You can inspect or log
        // the context variable to see the structure, but some key properties for
        // sheets are the actor object, the data object, whether or not it's
        // editable, the items array, and the effects array.
        const context = super.getData() as ActorSheet.Data<ActorSheet.Options>;

        // This is the perfect place to do the inversion of the condition
        // monitor for display in the character sheet. Except that we don't
        // want to track it as an upward counter everywhere else, that's
        // annoying. But we'll have to do it, probably. In which case we don't
        // need to convert...

        return context;
    }

    override activateListeners(html: JQuery): void {
        super.activateListeners(html);

        return;
    }

    override get template(): string {
        return 'systems/sr5e/dist/templates/actors/character.html';
    }
}
