export class SR5eItemSheet extends ItemSheet {
    static override get defaultOptions(): ItemSheet.Options {
        return mergeObject(super.defaultOptions, {
            classes: ["boilerplate", "sheet", "item"],
            width: 520,
            height: 480,
            tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
        });
    }

    override activateListeners(html: JQuery): void {
        if ((ui as any)['PDFoundry'] !== undefined) {
            $(html)
                .find('.open-source-pdf')
                .on('click', async (event) => {
                    event.preventDefault();
                    const match = event.currentTarget.innerText.trim().match(/^Source:\s+(?<code>\w+)\s+(?<page>\d+)/);
                    if (match === null || match.groups === undefined) {
                        return;
                    }

                    (ui as any).PDFoundry.openPDFByCode(
                        match.groups['code'],
                        { page: parseInt(match.groups['page'] as string) }
                    );
                });
        }
    }

    override get template(): string {
        return 'systems/sr5e/dist/templates/items/metatype.html';
    }
}
