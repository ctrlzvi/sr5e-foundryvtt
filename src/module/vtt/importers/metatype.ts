import { Metatype, isShapeshifter, Shapeshifter } from '../../system/models/metatype';

export class MetatypeImporter {
    // import imports the data into FoundryVTT items.
    async import(data: Metatype[], compendium: string): Promise<void> {
        const rootFolder = await this.getFolder(compendium);
        const metatypeFolder = await this.getFolder('Metatypes', rootFolder);

        for (const metatype of data) {
            if (isShapeshifter(metatype)) {
                if (await this.importShapeshifter(metatype as Shapeshifter, metatypeFolder) === undefined) {
                    throw new Error(`Failed to create MetatypeItem for ${metatype.shapeshifter?.metavariant.metasapient}`);
                }
            } else {
                if (await this.importMetatype(metatype, metatypeFolder) === undefined) {
                    throw new Error(`Failed to create MetatypeItem for ${metatype.metavariant.metasapient}`);
                }
            }
        }
    }

    async importMetatype(metatype: Metatype, folder: Folder): Promise<Item | undefined> {
        const metasapientFolder = await this.getFolder(metatype.metavariant.metasapient, folder);
        // TODO (zeffron 2021-09-12) If the item already exists, overwrite it
        // instead of creating a new one.
        return Item.create({
            name: metatype.metavariant.variant,
            type: 'metatype',
            data: metatype,
            folder: metasapientFolder.id,
        });
    }

    async importShapeshifter(metatype: Shapeshifter, folder: Folder): Promise<Item | undefined> {
        if (!isShapeshifter(metatype)) {
            throw new TypeError('non-Shapeshifter metatype passed to importShapeshifter');
        }
        // We can't nest deeper, FoundryVTT has a limit of 3 nesting layers. If
        // we go beyond that, everything breaks when it comes to folders.
        const breedFolder = await this.getFolder(metatype.shapeshifter.metavariant.metasapient, folder,
        );
        // TODO (zeffron 2021-09-12) If the item already exists, overwrite it
        // instead of creating a new one.
        return Item.create({
            name: metatype.metavariant.variant,
            type: 'metatype',
            data: metatype,
            folder: breedFolder.id,
        });
    }

    async getFolder(name: string, parent?: Folder): Promise<Folder> {
        const existingFolder = (game as Game).folders?.find(
            (folder) => folder.parentFolder == parent && folder.name == name
        );
        if (existingFolder !== undefined) {
            return existingFolder;
        }
        const newFolder = await Folder.create({
            type: 'Item',
            name: name,
            parent: parent?.id,
        });
        if (newFolder === undefined) {
            throw new Error(`Could not create folder ${name}`);
        }
        return newFolder;
    }
}
