import { SR5eActor } from './vtt/documents/actors/actor';
import { SR5eItem } from './vtt/documents/items/item';
import { SR5eActorSheet } from './vtt/sheets/actor';
import { SR5eItemSheet } from './vtt/sheets/item';
import { ChummerImport } from './vtt/applications/chummer-import';

export const SYSTEM_NAME = 'sr5e';

Hooks.once('init', () => {
    console.log(`Loading SR5e System
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMWNK00XWMMMMMMMMNOkkkOOOOOKWMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMWNKkdlc;'.',cd0XNMMMMO,........,xWMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMNOdl:'..   ........':xKWMO,.........,OWMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMNkc'.        ...... ..,dX0;. ...... .:XMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMWXOkkOOOd;.      .''',... ..,:.. .,'.'.. .dWMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMWKo,.....':c:'.    .,,'',,'.........,'.',.  .cx0NMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMW0l'.  ..     ........'...'''',,,,,,,,,''','... ..;oOXWMMMMMMMMMMMM
MMMMMMMMMMMW0l.     .........'',,,''''..................',;,''....,lONMMMMMMMMMM
MMMMMMMMMWOc.     ......'''',,'''...''.   ..'......''.. .,,..',,,,...;xXMMMMMMMM
MMMMMMMWOc.         ...... .';,..  .'..    ..............,.   .''',,...,xXMMMMMM
MMMMMMMKo:;;,'..    ....   .;,......'.....................  ...''...''. .:0WMMMM
MMMMMMMMWWX0xo:.  .....   .,;,'''.........   ......'..'''''''''..   ..'.  ,OWMMM
MMMMMMWXkl;'.... ..  .....',,..........         .','. .......'''....  .''. ;KMMM
MMMMMXo'.  .......   .....'...........          .''.         ...';;'.  .,. .xWMM
MMMMWx.   .......      .....''.'''.''...................         ..,'. .,. .dWMM
MMMMNl.    ......        .......,'...................'''......     .,,.'.  ,OMMM
MMMMK:..    ....              .''. .,coooooll:;,'.........'','.     .,'. .,kWMMM
MMMMO,...   ....              .... .;x0XXNWMMWWNNX0Oxdl:,.....       . .,oKWMMMM
MMMWd.....  .......         ............',:lx0XWMMMMMMMWNX0kdl:;,,,;:ldOXWMMMMMM
MMMXc.... ........'...'......   ...''''.......':oOXWMMMMMMMMMMWNNNNNWMMMMMMMMMMM
MMM0:'.. .''..............       ......''''''.....,lOXWMMMMMMMMMMMMMMMMMMMMMMMMM
MMMXd;.  ........               ...    .......',''...,dKWMMMMMMMMMMMMMMMMMMMMMMM
MMMMWk'   .'........          ...      .''..   ...''. .'oKWMMMMMMMMMMMMMMMMMMMMM
MMMMMNd.        ..''............       ...        ....   ,xNMMMMMMMMMMMMMMMMMMMM
MMMMMMNo.   .;l:.....................              .....  .oXMMMMMMMMMMMMMMMMMMM
MMMMMMMXl..'dXMWKxl,.   ..              ......  .........  .lXMMMMMMMMMMMMMMMMMM
MMMMMMMMXolOWMMMWKd;..............         ...  ..'.......  .dWMMMMMMMMMMMMMMMMM
MMMMMMMMMWWMMMW0l'..............            ... ....   ....  ,0MMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMKo.............      ..          ....      .'.  .dWMMMMMMMMMMMMMMMM
MMMMMMMMMMMWO;.....  .....   ..;oxkkdc'.       ..       .'..  cXMMMMMMMMMMMMMMMM
MMMMMMMMMMWO,..'.....''.    .:ONMMMMMW0c'.     ..    .  .''.  :XMMMMMMMMMMMMMMMM
MMMMMMMMMM0;.',.....''.    .cKMMMMMMMMMXOc.     ..  .....''.  cXMMMMMMMMMMMMMMMM
MMMMMMMMMXl..,.    .''.   .,OMMMMMMMMMMWWd.     .  ........  .oNMMMMMMMMMMMMMMMM
MMMMMMMMMO' .'......,'   ..;KMMMMMMMMMMWNd.   ......  .....  .kMMMMMMMMMMMMMMMMM
MMMMMMMMWd. .'......,'   ..'OMMMMMMMMMW0d;.   ',..     ...   :XMMMMMMMMMMMMMMMMM
MMMMMMMMWd. ..     .',.  ...:0WMMMMMMWO;..   .''.     ....  .kWMMMMMMMMMMMMMMMMM
MMMMMMMMWx. ..    ..','.  ...,oOKXX0kc'..   .'..     ....  .oNMMMMMMMMMMMMMMMMMM
MMMMMMMMMK;  .. .... .''... ....','...   ..''.       ...  .oNMMMMMMMMMMMMMMMMMMM
MMMMMMMMMWd. .....    ..,;,... ........''''..       ...  .dNMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMNl. ...     .',..''''''''''.....        ....  ;OWMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMXo. ....   .,.    ..';'..           .....  'oXMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMNx,  ......'.      .,.          ......  'oKWMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMWKo'   ..''.       .''.. .........  .;dKWMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMWXx;.   ..........',,'......   .;oONMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMW0dc,.    ........    ...;lx0NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMWNKkdlc:;;;;;;:codkO0XWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMWWNNNNWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    `);
    // Define custom Document classes
    CONFIG.Actor.documentClass = SR5eActor;
    CONFIG.Item.documentClass = SR5eItem;

    // Register sheet application classes
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("sr5e", SR5eActorSheet, { makeDefault: true });
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("sr5e", SR5eItemSheet, { makeDefault: true });

    // Preload Handlebars templates.
    // return preloadHandlebarsTemplates();

    (game as Game).settings.register(
        SYSTEM_NAME,
        'SR5e.SourceBooks',
        {
            name: 'SETTINGS.SourceBooks.Name',
            hint: 'SETTINGS.SourceBooks.Hint',
            scope: 'world',
            config: false,
            type: Object,
            default: {},
        },
    );

    (game as Game).settings.registerMenu(
        SYSTEM_NAME,
        'chummerDataMenu',
        {
            name: 'SETTINGS.ImportChummerData',
            label: 'SETTINGS.ImportChummerData.Label',
            hint: 'SETTINGS.ImportChummerDataHint',
            icon: 'fas fa-database',
            type: ChummerImport,
            restricted: true,

        },
    );

    Handlebars.registerHelper('debugPrint', function (obj) {
        console.log(obj);
    });

    return loadTemplates([
        'systems/sr5e/dist/templates/source.html',
    ]);
});

/* -------------------------------------------- */
/*  Ready Hook                                  */
/* -------------------------------------------- */

Hooks.once("ready", function () {
    // Include steps that need to happen after Foundry has fully loaded here.
});

export { };