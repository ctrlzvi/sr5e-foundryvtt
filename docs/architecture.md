# Code organization

Everything is inside one root module, the sr5e module.
The module is broken into three components:

  - Game system rules in `system`
  - FoundryVTT integration in `vtt`
  - Chummer import related things in `chummer`

# Values

One of the main issues I had with the Shadowrun 5th Edition game system that
led to me playing around with making my own is that it has issues with effects
modifying attributes, which I was trying to use to make, e.g. wired reflexes
work correctly.

To mitigate this, I'm trying to approach things with a very simple formula:
Actor data is split into two categories: Base and Derived (which is equivalent to how
FoundryVTT splits data, anyway).

Base data is data inherent to the actor. E.g. for attributes this is the number
of points the player has purchased. For essence, this is always 6, etc.

Any modifications to the base data, for any reason whatsoever, will be treated
as derived data. This includes things like applying limits.
